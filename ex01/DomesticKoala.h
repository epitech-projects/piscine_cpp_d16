/*
** DomesticKoala.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d16/ex01
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Thu Jan 23 21:20:33 2014 Jean Gravier
** Last update Fri Jan 24 02:52:22 2014 Jean Gravier
*/

#ifndef _DOMESTICKOALA_H_
#define _DOMESTICKOALA_H_

#include <vector>
#include "KoalaAction.h"

class					DomesticKoala
{
public:
  DomesticKoala(KoalaAction &);
  ~DomesticKoala();
  DomesticKoala(const DomesticKoala&);
  DomesticKoala& operator=(const DomesticKoala&);

public:
  typedef void (KoalaAction::* methodPointer_t)(std::string const&);
  std::vector<methodPointer_t> const*	getActions(void) const;

public:
  void					learnAction(unsigned char, methodPointer_t);
  void					unlearnAction(unsigned char);
  void					doAction(unsigned char, const std::string&);
  void					setKoalaAction(KoalaAction&);
  std::vector<methodPointer_t>		getVector() const;
  std::vector<unsigned char>		getOp() const;

private:
  std::vector<methodPointer_t>		_methodsPtr;
  std::vector<unsigned char>		_op;
  KoalaAction				_action;
};

#endif /* _DOMESTICKOALA_H_ */
