//
// DomesticKoala.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d16/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Jan 23 18:13:23 2014 Jean Gravier
// Last update Fri Jan 24 02:52:03 2014 Jean Gravier
//

#include <iostream>
#include <string>
#include <vector>
#include "DomesticKoala.h"

DomesticKoala::DomesticKoala(KoalaAction &action): _action(action)
{

}

DomesticKoala::~DomesticKoala()
{
  this->_methodsPtr.clear();
}

DomesticKoala::DomesticKoala(DomesticKoala const& koala)
{
  this->_methodsPtr = koala.getVector();
  this->_op = koala.getOp();
}

DomesticKoala		&DomesticKoala::operator=(DomesticKoala const& koala)
{
  this->_methodsPtr = koala.getVector();
  this->_op = koala.getOp();
  return (*this);
}

std::vector<DomesticKoala::methodPointer_t>	DomesticKoala::getVector() const
{
  return (this->_methodsPtr);
}

std::vector<unsigned char>	DomesticKoala::getOp() const
{
  return (this->_op);
}

void			DomesticKoala::learnAction(unsigned char ch, methodPointer_t ptr)
{
  unsigned int		i;

  for (i = 0; i < this->_op.size(); ++i)
    {
      if (this->_op[i] == ch)
	{
	  this->_op[i] = ch;
	  this->_methodsPtr[i] = ptr;
	  return ;
	}
    }
  this->_op.push_back(ch);
  this->_methodsPtr.push_back(ptr);
}

void			DomesticKoala::unlearnAction(unsigned char op)
{
  unsigned int		i;

  for (i = 0; i < this->_op.size(); ++i)
    {
      if (this->_op[i] == op)
	{
	  this->_op.erase(this->_op.begin() + i);
	  this->_methodsPtr.erase(this->_methodsPtr.begin() + i);
	}
    }
}

void			DomesticKoala::doAction(unsigned char op, std::string const& str)
{
  unsigned int		i;

  for (i = 0; i < this->_op.size(); ++i)
    {
      if (this->_op[i] == op)
	((this->_action).*(_methodsPtr[i]))(str);
    }
}

void			DomesticKoala::setKoalaAction(KoalaAction &action)
{
  this->_action = action;
}

std::vector<DomesticKoala::methodPointer_t> const*	DomesticKoala::getActions(void) const
{
  return (&this->_methodsPtr);
}
